import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  
  products: any;
  subTotal: any = 0;
  vat: any = 0;
  totalCost: any = 0;
  productsPrice:any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get('assets/json/products.json').subscribe((res) => {
      this.products = res;
      this.products = this.products['items'];
      this.refreshTotal()
    });
  }

  removeProduct(sku:any) {
    let removed = this.products.filter(function(item:any){ 
      return item.sku != sku;
    });
    this.products = removed
    this.refreshTotal();
  }

  refreshTotal() {
    this.productsPrice = []
    this.products.forEach((product: { price: any; quantity: any; }) => {
      this.productsPrice.push(product.price * product.quantity)
    });
  
    this.subTotal = this.productsPrice.reduce((a:any, b:any) => a + b, 0)
    this.vat = this.subTotal * (1 + 0.20) - this.subTotal
    this.totalCost = this.subTotal + this.vat
    this.totalCost = this.totalCost.toFixed(2)
    this.vat = this.vat.toFixed(2)
    this.subTotal = this.subTotal.toFixed(2)
  }

  increase (sku:any) {
    let product = this.products.filter(function(item:any){ 
      return item.sku == sku;
    });
    if(product[0].quantity < product[0].stockLevel) {
      product[0].quantity++
      this.refreshTotal()
    }
  }

  decrease (sku:any) {
    let product = this.products.filter(function(item:any){ 
      return item.sku == sku;
    });
    if(product[0].quantity > 1) {
      product[0].quantity--
      this.refreshTotal()
    }
  }

}
